declare @SourceTable1Name nvarchar(50)
set @SourceTable1Name = 'SourceTable1'

IF (EXISTS (SELECT * 
                 FROM INFORMATION_SCHEMA.TABLES 
                 WHERE TABLE_SCHEMA = 'dbo' 
                 AND  TABLE_NAME = @SourceTable1Name))
	BEGIN
		print(@SourceTable1Name + ' table exists')
	END
ELSE
	BEGIN
		print(@SourceTable1Name + ' table created')
		EXEC('CREATE TABLE ' + @SourceTable1Name + '(
		[socialsecuritynumber] nvarchar(50) NOT NULL,
		[Firstname] nvarchar(50) NOT NULL, 
		[Lastname] nvarchar(50) NOT NULL, 
		[Department] nvarchar(50) NOT NULL, 
		PRIMARY KEY ([socialsecuritynumber]))')
	END
	
declare @SourceTable2Name nvarchar(50)
set @SourceTable2Name = 'SourceTable2'
IF (EXISTS (SELECT * 
                FROM INFORMATION_SCHEMA.TABLES 
                WHERE TABLE_SCHEMA = 'dbo' 
                AND  TABLE_NAME = @SourceTable2Name))
	BEGIN
		print(@SourceTable2Name + ' table exists')
	END
ELSE
	BEGIN
		print(@SourceTable2Name + ' table created')
		EXEC('CREATE TABLE ' + @SourceTable2Name + '(
		[socialsecuritynumber] nvarchar(50) NOT NULL,
		[Firstname] nvarchar(50) NOT NULL, 
		[Lastname] nvarchar(50) NOT NULL, 
		[Department] nvarchar(50) NOT NULL, 
		PRIMARY KEY ([socialsecuritynumber]))')
	END


IF NOT EXISTS (SELECT * FROM [SourceTable1] WHERE [socialsecuritynumber] = '01010101')
	BEGIN
		INSERT INTO [SourceTable1]
           ([socialsecuritynumber]
           ,[Firstname]
           ,[Lastname]
           ,[Department])
		VALUES
           ('01010101'
           ,'Kari'
           ,'Nordmann'
           ,'Sales')
	END
ELSE
	print('value exists')

IF NOT EXISTS (SELECT * FROM [SourceTable1] WHERE [socialsecuritynumber] = '01010102')
	BEGIN
		INSERT INTO [SourceTable1]
           ([socialsecuritynumber]
           ,[Firstname]
           ,[Lastname]
           ,[Department])
		VALUES
           ('01010102'
           ,'Jack'
           ,'Jackson'
           ,'Support')
	END
ELSE
	print('value exists')

IF NOT EXISTS (SELECT * FROM [SourceTable1] WHERE [socialsecuritynumber] = '01010103')
	BEGIN
		INSERT INTO [SourceTable1]
           ([socialsecuritynumber]
           ,[Firstname]
           ,[Lastname]
           ,[Department])
		VALUES
           ('01010103'
           ,'Nils'
           ,'Nilsen'
           ,'Sales')
	END
ELSE
	print('value exists')




IF NOT EXISTS (SELECT * FROM [SourceTable2] WHERE [socialsecuritynumber] = '01010101')
	BEGIN
		INSERT INTO [SourceTable2]
           ([socialsecuritynumber]
           ,[Firstname]
           ,[Lastname]
           ,[Department])
		VALUES
           ('01010101'
           ,'Kari'
           ,'Nordman'
           ,'Support')
	END
ELSE
	print('value exists')

IF NOT EXISTS (SELECT * FROM [SourceTable2] WHERE [socialsecuritynumber] = '0101004')
	BEGIN
		INSERT INTO [SourceTable2]
           ([socialsecuritynumber]
           ,[Firstname]
           ,[Lastname]
           ,[Department])
		VALUES
           ('0101004'
           ,'Esther'
           ,'Doe'
           ,'Support')
	END
ELSE
	print('value exists')

IF NOT EXISTS (SELECT * FROM [SourceTable2] WHERE [socialsecuritynumber] = '01010103')
	BEGIN
		INSERT INTO [SourceTable2]
           ([socialsecuritynumber]
           ,[Firstname]
           ,[Lastname]
           ,[Department])
		VALUES
           ('01010103'
           ,'Nils'
           ,'Nilsen'
           ,'Sales')
	END
ELSE
	print('value exists')
