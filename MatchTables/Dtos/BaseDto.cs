﻿using System.Collections.Generic;

namespace MatchTables.Dtos
{
    public class BaseDto
    {
        public string ColumnName { get; set; }
        public object ColumnValue { get; set; }
    }

    public class BasicItemOperationDto : BaseDto
    {
        public List<BaseDto> PropertyColumnWithValues { get; set; }
    }
}