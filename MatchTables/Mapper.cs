﻿using System;
using System.Collections.Generic;
using System.Linq;
using MatchTables.Dtos;

namespace MatchTables
{
    public static class Mapper
    {
        public static IEnumerable<AdditionItemDto> MapToAdditionDto(this List<Dictionary<string, object>> itemList,
            string primaryKey)
        {
            var additionItemDtoList = MapToBasicOperationalDto<AdditionItemDto>(itemList, primaryKey);

            return additionItemDtoList;
        }

        public static IEnumerable<RemovalItemDto> MapToRemovalDto(this List<Dictionary<string, object>> itemList,
            string primaryKey)
        {
            var removalItemDtoList = MapToBasicOperationalDto<RemovalItemDto>(itemList, primaryKey);

            return removalItemDtoList;
        }

        /// <summary>
        /// Maps the property changes between two tables into list if there's any changes
        /// </summary>
        /// <param name="occuredChangesList"></param>
        /// <param name="changedPropertiesList"></param>
        /// <param name="primaryKey"></param>
        /// <returns></returns>
        public static IEnumerable<AlterationItemDto> MapToAlterationDto(
            this List<Dictionary<string, object>> occuredChangesList,
            List<Dictionary<string, object>> changedPropertiesList, string primaryKey)
        {
            var currentIndex = 0;
            var alterationItemDtoList = new List<AlterationItemDto>();

            foreach (var tableSingleRowDict in occuredChangesList
                .TakeWhile(tableSingleRowDict => changedPropertiesList.Count >= currentIndex))
            {
                alterationItemDtoList.AddRange(from tableSingleRowKey in tableSingleRowDict.Keys
                                           where !primaryKey.Equals(tableSingleRowKey, StringComparison.InvariantCultureIgnoreCase) &&
                                                 changedPropertiesList[currentIndex].ContainsKey(tableSingleRowKey) &&
                                                 !changedPropertiesList[currentIndex][tableSingleRowKey]
                                                     .Equals(tableSingleRowDict[tableSingleRowKey])
                                           select new AlterationItemDto()
                                           {
                                               PrimaryKeyValue = tableSingleRowDict[primaryKey],
                                               ColumnName = tableSingleRowKey,
                                               PreviousValue = tableSingleRowDict[tableSingleRowKey],
                                               NewValue = changedPropertiesList[currentIndex][tableSingleRowKey]
                                           });
                currentIndex++;
            }

            return alterationItemDtoList;
        }

        private static IEnumerable<T> MapToBasicOperationalDto<T>(
            List<Dictionary<string, object>> itemList, string primaryKey)
            where T : BasicItemOperationDto, new()
        {
            var basicItemOperationDtoList = new List<T>();
            itemList.ForEach(currentRow =>
            {
                var basicItemOperationDto = new T();
                var baseDtoList = new List<BaseDto>();
                foreach (var key in currentRow.Keys)
                {
                    if (key.Equals(primaryKey, StringComparison.InvariantCultureIgnoreCase))
                    {
                        basicItemOperationDto.ColumnName = primaryKey;
                        basicItemOperationDto.ColumnValue = currentRow[primaryKey];
                        continue;
                    }

                    baseDtoList.Add(new BaseDto()
                    {
                        ColumnName = key,
                        ColumnValue = currentRow[key]
                    });
                }

                basicItemOperationDto.PropertyColumnWithValues = baseDtoList;
                basicItemOperationDtoList.Add(basicItemOperationDto);
            });

            return basicItemOperationDtoList;
        }

    }
}