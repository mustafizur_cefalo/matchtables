﻿namespace MatchTables.TableMatcher
{
    public interface ITableMatcher
    {
        string GetAddedItems(string primaryKey);
        string GetRemovedItems(string primaryKey);
        string GetChangedPropertyOfItems(string primaryKey);
    }
}