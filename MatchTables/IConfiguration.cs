﻿namespace MatchTables
{
    public interface IConfiguration
    {
        string GetConnectionString(string connectionName = "dbConnection");
    }
}