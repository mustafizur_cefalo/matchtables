﻿using System.Collections.Generic;
using System.Linq;
using System.Text;
using MatchTables.Services;
using MatchTables.Dtos;

namespace MatchTables.TableMatcher
{
    public class TableMatcher : ITableMatcher
    {
        private readonly IMatcherService _matcherService;
        private readonly string _oldTableName;
        private readonly string _newTableName;

        public TableMatcher(IMatcherService matcherService, string oldTableName, string newTableName)
        {
            _matcherService = matcherService;

            _oldTableName = oldTableName;
            _newTableName = newTableName;
        }

        public string GetAddedItems(string primaryKey)
        {
            var addedItemDtoList = _matcherService.GetAddedItems(_oldTableName, _newTableName, primaryKey);
            var addedItemsAsString = GetItemsAsString(addedItemDtoList);

            if (string.IsNullOrEmpty(addedItemsAsString))
                addedItemsAsString = "No Row Added.";

            return addedItemsAsString;
        }

        public string GetRemovedItems(string primaryKey)
        {
            var removedItemDtoList = _matcherService.GetRemovedItems(_oldTableName, _newTableName, primaryKey);
            var removedItemsAsString = GetItemsAsString(removedItemDtoList);

            if (string.IsNullOrEmpty(removedItemsAsString))
                removedItemsAsString = "No Row Removed.";

            return removedItemsAsString;
        }

        public string GetChangedPropertyOfItems(string primaryKey)
        {
            var alterationItemDtoList = _matcherService.GetAlteredItems(_oldTableName, _newTableName, primaryKey);
            var stringBuilderAlteredItems = new StringBuilder();
            foreach (var alterationItemDto in alterationItemDtoList)
            {
                stringBuilderAlteredItems.AppendLine(
                    $"   • {alterationItemDto.PrimaryKeyValue} {alterationItemDto.ColumnName} has changed from {alterationItemDto.PreviousValue} to {alterationItemDto.NewValue} ");
            }

            if (string.IsNullOrEmpty(stringBuilderAlteredItems.ToString()))
                stringBuilderAlteredItems.AppendLine("No Property Changed.");

            return stringBuilderAlteredItems.ToString();
        }

        private string GetItemsAsString(IEnumerable<BasicItemOperationDto> basicItemOperationDtoList)
        {
            var stringBuilder = new StringBuilder();
            foreach (var basicItemOperationDto in basicItemOperationDtoList)
            {
                var totalColumnIterate = 0;
                stringBuilder.Append($"   • {basicItemOperationDto.ColumnValue} (");
                foreach (var baseDto in basicItemOperationDto.PropertyColumnWithValues
                    .TakeWhile(values => totalColumnIterate < 2))
                {
                    stringBuilder.Append($"{baseDto.ColumnValue} ");
                    totalColumnIterate++;
                }

                var trimmedString = stringBuilder.ToString().TrimEnd(' ');
                stringBuilder.Clear();
                stringBuilder.Append(trimmedString + ")\n");
            }

            return stringBuilder.ToString();
        }
    }
}