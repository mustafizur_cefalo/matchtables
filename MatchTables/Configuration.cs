﻿using System.Configuration;

namespace MatchTables
{
    public class Configuration : IConfiguration
    {
        public Configuration() { }

        public string GetConnectionString(string connectionName = "dbConnection")
        {
            return ConfigurationManager.ConnectionStrings[connectionName].ConnectionString;
        }
    }
}