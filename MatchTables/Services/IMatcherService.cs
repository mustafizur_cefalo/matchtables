﻿using System.Collections.Generic;
using MatchTables.Dtos;

namespace MatchTables.Services
{
    public interface IMatcherService
    {
        IEnumerable<AdditionItemDto> GetAddedItems(string oldTableName, string newTableName, string primaryKey);
        IEnumerable<RemovalItemDto> GetRemovedItems(string oldTableName, string newTableName, string primaryKey);
        IEnumerable<AlterationItemDto> GetAlteredItems(string oldTableName, string newTableName, string primaryKey);
    }
}