
# MatchTables #

Necessary steps are given below to get application up and running.

### What is this repository for? ###

* Changes between two identical schema tables
* version 1.0

### Clone

* To clone the repo to your local machine run this command. (You must have git installed in your local machine)
```
git clone https://mustafizur_cefalo@bitbucket.org/mustafizur_cefalo/matchtables.git
```

### How do I get set up? ###

* **Summary of set up**
	* Please make sure you have .Net Framework 4.7.2 and SQL Server on local machine.
	
	
* **Database configuration**
	* Before running the application, change connectionString in App.config
		* Default connectionString is 
		```
		Data Source=localhost;Initial Catalog=dni-assignment;Integrated Security=SSPI;
		```
		
		* Change it to your local machine sql connection string if required
	### SQL Scripts
	* Run 
		**databaseCreateMandatory.sql** first, this will create a database name of `dni-assignment`.
	
	* Then run 
		**tableCreateInsertAssignmentReplica.sql** to create tables and insert data into the tables.
		```This will create exactly same table and data given in the example of the assignment. Named :
		SourceTable1
		SourceTable2```
	
	* If you want to try for different schema's table but in those tables schema same. Then run 
		**tableCreateInsertSample.sql** it will create table and insert data into the database.
		```This will create table Named :
		DiffSource &
		DiffTarget
		Also these:
		TableSource &
		TableTarget```
	
	* If you want to try for huge datasets but in those tables schema same. Then run 
		**tableCreateInsertHugeData.sql** it will create table and insert data into the database.
		Aproximately **12000** rows.
		```This will create table Named :
		tblAuthorsSource &
		tblAuthorsTarget
		Also these: 
		StudentTable_Source &
		StudentTable_Target```
	
	* **Those sql scripts are already given in the repository**.
	
	
* **Deployment instructions**
	* Console Input
		* Run 
			**MatchTables.exe**
			```
			Table1  YourTable1Name
			Table2 YourTable2Name
			PrimaryKey YourTablePrimaryKeyName
			```
	* Run using command-line args
		* Right-Click on Project. Go to Properties -> Debug -> Command-line-arguments
		
		![alt text](https://i.ibb.co/kcQMwF9/Command-Line-Args.png)
		
		* Enter
		`-Table1 SourceTable1 -Table2 SourceTable2 -Primarykey SocialSecuritynumber`
		Or
		`-Table1 tblAuthorsSource -Table2 tblAuthorsTarget -Primarykey Id`
		Then run **MatchTables.exe**

### Who do I talk to? ###

* Mustafizur Rhaman Shakil, mustafizur@cefalo.com

### License

This project is licensed under the MIT License