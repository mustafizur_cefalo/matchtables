﻿using System;
using System.Collections.Generic;
using System.IO;
using MatchTables.Repository;
using MatchTables.Dtos;

namespace MatchTables.Services
{
    public class MatcherService : IMatcherService
    {
        private readonly IRepository _repository;

        public MatcherService(IRepository repository)
        {
            _repository = repository;
        }

        public IEnumerable<AdditionItemDto> GetAddedItems(string oldTableName, string newTableName, string primaryKey)
        {
            CheckTableName(oldTableName, newTableName);
            var queryAsString = $"SELECT [{newTableName}].* FROM [{newTableName}]"
                                + $" LEFT JOIN [{oldTableName}] ON"
                                + $" ([{newTableName}].[{primaryKey}] = [{oldTableName}].[{primaryKey}])"
                                + $" where [{oldTableName}].[{primaryKey}] IS NULL";

            var addedItems = _repository.FindAll(queryAsString);

            return addedItems.MapToAdditionDto(primaryKey);
        }

        public IEnumerable<RemovalItemDto> GetRemovedItems(string oldTableName, string newTableName, string primaryKey)
        {
            CheckTableName(oldTableName, newTableName);
            var queryAsString = $"SELECT [{oldTableName}].* FROM [{newTableName}]" +
                                $" RIGHT JOIN [{oldTableName}]" +
                                $" ON ([{newTableName}].[{primaryKey}] = [{oldTableName}].[{primaryKey}])" +
                                $" where [{newTableName}].[{primaryKey}] IS NULL";
            var removedItems = _repository.FindAll(queryAsString);

            return removedItems.MapToRemovalDto(primaryKey);
        }

        public IEnumerable<AlterationItemDto> GetAlteredItems(string oldTableName, string newTableName, string primaryKey)
        {
            CheckTableName(oldTableName, newTableName);
            var queryExceptAsString = $"SELECT [{oldTableName}].* FROM [{oldTableName}]"
                                      + $" INNER JOIN [{newTableName}] ON"
                                      + $" ([{oldTableName}].[{primaryKey}] = [{newTableName}].[{primaryKey}])"
                                      + " EXCEPT"
                                      + $" SELECT [{newTableName}].* FROM [{oldTableName}]"
                                      + $" INNER JOIN [{newTableName}]"
                                      + $" ON ([{oldTableName}].[{primaryKey}] = [{newTableName}].[{primaryKey}])"
                                      + $" ORDER BY [{primaryKey}]";

            var occuredChangesList = _repository.FindAll(queryExceptAsString);

            if (occuredChangesList.Count <= 0)
                return new List<AlterationItemDto>();

            var primaryKeysValueAsString = string.Empty;
            occuredChangesList.ForEach(x => { primaryKeysValueAsString += $"'{x[primaryKey]}' ,"; });

            var queryInAsString = $"SELECT * FROM [{newTableName}]" +
                                  $" WHERE [{newTableName}].[{primaryKey}] IN ({primaryKeysValueAsString.TrimEnd(',')})"
                                  + $" ORDER BY [{primaryKey}]";
            var changedPropertiesList = _repository.FindAll(queryInAsString);

            if (changedPropertiesList.Count != occuredChangesList.Count)
                throw new Exception("Failed to retrieved modified records, Please contact the administrator.");

            return occuredChangesList.MapToAlterationDto(changedPropertiesList, primaryKey);
        }

        private void CheckTableName(string oldTableName, string newTableName)
        {
            if (string.IsNullOrEmpty(oldTableName) || string.IsNullOrEmpty(newTableName))
                throw new InvalidDataException("Set table names. Table name can not be empty.");

            if (oldTableName.Equals(newTableName, StringComparison.InvariantCultureIgnoreCase))
                throw new InvalidDataException("Can not compare same tables.");
        }
    }
}