﻿namespace MatchTables.Dtos
{
    public class AlterationItemDto
    {
        public object PrimaryKeyValue { get; set; }
        public string ColumnName { get; set; }
        public object PreviousValue { get; set; }
        public object NewValue { get; set; }
    }
}