﻿using System.Collections.Generic;

namespace MatchTables.Repository
{
    public interface IRepository
    {
        List<Dictionary<string, object>> FindAll(string query);
        bool HasColumn(string tableName, string columnName);
        bool IsIdentical(string oldTableName, string newTableName);
    }
}