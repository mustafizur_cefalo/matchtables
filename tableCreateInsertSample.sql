USE [dni-assignment]
GO
/****** Object:  Table [dbo].[DiffSource]    Script Date: 4/30/2020 11:58:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiffSource](
	[id] [nvarchar](50) NOT NULL,
	[age] [smallint] NOT NULL,
	[height] [nvarchar](50) NOT NULL,
	[gender] [nvarchar](50) NOT NULL,
	[balance] [decimal](18, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[DiffTarget]    Script Date: 4/30/2020 11:58:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[DiffTarget](
	[id] [nvarchar](50) NOT NULL,
	[age] [smallint] NOT NULL,
	[height] [nvarchar](50) NOT NULL,
	[gender] [nvarchar](50) NOT NULL,
	[balance] [decimal](18, 2) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TableSource]    Script Date: 4/30/2020 11:58:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableSource](
	[socialsecuritynumber] [nvarchar](50) NOT NULL,
	[Firstname] [nvarchar](50) NOT NULL,
	[Lastname] [nvarchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[socialsecuritynumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[TableTarget]    Script Date: 4/30/2020 11:58:52 AM ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[TableTarget](
	[socialsecuritynumber] [nvarchar](50) NOT NULL,
	[Firstname] [nvarchar](50) NOT NULL,
	[Lastname] [nvarchar](50) NOT NULL,
	[Department] [nvarchar](50) NOT NULL,
PRIMARY KEY CLUSTERED 
(
	[socialsecuritynumber] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
INSERT [dbo].[DiffSource] ([id], [age], [height], [gender], [balance]) VALUES (N'1', 20, N'167cm', N'Male', CAST(10.20 AS Decimal(18, 2)))
INSERT [dbo].[DiffSource] ([id], [age], [height], [gender], [balance]) VALUES (N'2', 21, N'169cm', N'Male', CAST(0.20 AS Decimal(18, 2)))
INSERT [dbo].[DiffSource] ([id], [age], [height], [gender], [balance]) VALUES (N'3', 10, N'127cm', N'Male', CAST(11.20 AS Decimal(18, 2)))
INSERT [dbo].[DiffTarget] ([id], [age], [height], [gender], [balance]) VALUES (N'2', 25, N'159cm', N'Female', CAST(69.20 AS Decimal(18, 2)))
INSERT [dbo].[DiffTarget] ([id], [age], [height], [gender], [balance]) VALUES (N'3', 10, N'110cm', N'Male', CAST(11.20 AS Decimal(18, 2)))
INSERT [dbo].[DiffTarget] ([id], [age], [height], [gender], [balance]) VALUES (N'5', 15, N'137cm', N'Male', CAST(11.20 AS Decimal(18, 2)))
INSERT [dbo].[TableSource] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010101', N'Kari', N'Nordmann', N'Sales')
INSERT [dbo].[TableSource] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010102', N'Jack', N'Jackson', N'Support')
INSERT [dbo].[TableSource] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010103', N'Nils', N'Nilsen', N'Sales')
INSERT [dbo].[TableSource] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010104', N'John', N'Doe', N'Special Force')
INSERT [dbo].[TableSource] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010105', N'Jane', N'Doe', N'None')
INSERT [dbo].[TableSource] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010106', N'Riaz', N'Khan', N'Actor')
INSERT [dbo].[TableSource] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010107', N'Shakib', N'Al Hasan', N'Cricketer')
INSERT [dbo].[TableSource] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010108', N'Meena', N'Raju', N'Cartoon')
INSERT [dbo].[TableTarget] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'0101001', N'Tamim', N'Iqbal', N'Cricketer')
INSERT [dbo].[TableTarget] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'0101004', N'Esther', N'Doe', N'Support')
INSERT [dbo].[TableTarget] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010101', N'Kari', N'Nordman', N'Support')
INSERT [dbo].[TableTarget] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010103', N'Nils', N'Nilsen', N'Sales')
INSERT [dbo].[TableTarget] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010104', N'Happy', N'Singh', N'Guard')
INSERT [dbo].[TableTarget] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010105', N'Danny', N'Smith', N'None')
INSERT [dbo].[TableTarget] ([socialsecuritynumber], [Firstname], [Lastname], [Department]) VALUES (N'01010107', N'Shakib Al', N'Hasan', N'Cricketer')
