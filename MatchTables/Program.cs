﻿using System;
using MatchTables.Services;

namespace MatchTables
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.OutputEncoding = System.Text.Encoding.UTF8;


            IConfiguration configuration = new Configuration();
            var connectionString = configuration.GetConnectionString();

            try
            {
                string oldTableName;
                string newTableName;
                string primaryKey;

                if (args != null && args.Length > 5)
                {
                    Console.WriteLine("Reading from args ... ");
                    oldTableName = args[1];
                    newTableName = args[3];
                    primaryKey = args[5];
                }
                else
                {
                    Console.WriteLine("Reading from console ... ");
                    oldTableName = ReadInput("-Table1 ");
                    newTableName = ReadInput("-Table2 ");
                    primaryKey = ReadInput("-PrimaryKey ");
                }

                Console.WriteLine("\n");

                var repository = new Repository.Repository(connectionString);
                if(!repository.IsIdentical(oldTableName, newTableName))
                    throw new Exception("Table schemas are not identical. Can not compare between these two tables.");
                var tableMatcher =
                    new TableMatcher.TableMatcher(new MatcherService(repository), oldTableName, newTableName);

                if (!repository.HasColumn(oldTableName, primaryKey) ||
                    !repository.HasColumn(newTableName, primaryKey))
                {
                    throw new Exception("Key does not exists in the table");
                }

                ShowOutput("Added records:", tableMatcher.GetAddedItems(primaryKey));
                ShowOutput("\n\nRemoved records:", tableMatcher.GetRemovedItems(primaryKey));
                ShowOutput("\n\nChanges:", tableMatcher.GetChangedPropertyOfItems(primaryKey));
            }
            catch (Exception exception)
            {
                Console.ForegroundColor = ConsoleColor.DarkRed;
                Console.WriteLine(exception.Message + "\n\n\nTry Again ... ");
            }

            Console.ReadLine();
        }

        private static void ShowOutput(string labelHeader, string outputValue)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(labelHeader);
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine(outputValue);
        }

        private static string ReadInput(string inputName)
        {
            Console.Write(inputName);
            var readInput = Console.ReadLine();
            return readInput;
        }
    }
}
