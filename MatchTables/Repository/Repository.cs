﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;

namespace MatchTables.Repository
{
    public class Repository : IRepository
    {
        private readonly string _connectionString;

        public Repository(string connectionString)
        {
            _connectionString = connectionString;
        }

        /// <summary>
        /// retrieves data from table according to the specific query
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public List<Dictionary<string, object>> FindAll(string query)
        {
            var tableAsList = new List<Dictionary<string, object>>();
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                sqlConnection.Open();

                using (var sqlCommand = new SqlCommand(query, sqlConnection))
                {
                    using (var sqlDataReader = sqlCommand.ExecuteReader())
                    {
                        while (sqlDataReader.Read())
                        {
                            var tableRowAsKeyValuePair =
                                new Dictionary<string, object>(StringComparer.InvariantCultureIgnoreCase);
                            for (var currentIndex = 0; currentIndex < sqlDataReader.FieldCount; currentIndex++)
                            {
                                tableRowAsKeyValuePair.Add(sqlDataReader.GetName(currentIndex),
                                    sqlDataReader.GetValue(currentIndex));
                            }

                            tableAsList.Add(tableRowAsKeyValuePair);
                        }
                    }
                }
            }

            return tableAsList;
        }

        public bool IsIdentical(string oldTableName, string newTableName)
        {
            bool isIdentical;
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var query = "SELECT CASE WHEN EXISTS ("
                            + $" SELECT name, system_type_id, user_type_id,max_length, precision,scale, is_nullable, is_identity"
                            + $" FROM sys.columns WHERE object_id = OBJECT_ID('{oldTableName}')"
                            + " EXCEPT"
                            + $" SELECT name, system_type_id, user_type_id,max_length, precision,scale, is_nullable, is_identity"
                            + $" FROM sys.columns WHERE object_id = OBJECT_ID('{newTableName}'))"
                            + " THEN CAST(0 AS BIT)"
                            + " ELSE CAST(1 AS BIT) END";

                sqlConnection.Open();

                using (var sqlCommand = new SqlCommand(query, sqlConnection))
                {
                    isIdentical = (bool) sqlCommand.ExecuteScalar();
                }
            }

            return isIdentical;
        }

        /// <summary>
        /// Checks whether A table has specified column
        /// </summary>
        /// <param name="tableName"></param>
        /// <param name="columnName"></param>
        /// <returns></returns>
        public bool HasColumn(string tableName, string columnName)
        {
            bool hasColumn;
            using (var sqlConnection = new SqlConnection(_connectionString))
            {
                var query = "SELECT CASE WHEN EXISTS ("
                            + "SELECT Name FROM sys.columns WHERE"
                            + $" Name = '{columnName}' AND Object_ID = Object_ID('{tableName}'))"
                            + " THEN CAST(1 AS BIT)"
                            + " ELSE CAST(0 AS BIT) END";

                sqlConnection.Open();

                using (var sqlCommand = new SqlCommand(query, sqlConnection))
                {
                    hasColumn = (bool)sqlCommand.ExecuteScalar();
                }
            }

            return hasColumn;
        }
    }
}